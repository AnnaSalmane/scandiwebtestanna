$("#productType").change(function () {
    switch($("#productType").val()) {
        case "dvd":
            $("#dvd").css("display", "inline")
            $("#book").css("display", "none")
            $("#furniture").css("display", "none")
        break
        case "book":
            $("#dvd").css("display", "none")
            $("#book").css("display", "inline")
            $("#furniture").css("display", "none")
        break
        case "furniture":
            $("#dvd").css("display", "none")
            $("#book").css("display", "none")
            $("#furniture").css("display", "inline")
        break
      default:
        $("#book").css("display", "none")
        $("#dvd").css("display", "none")
        $("#furniture").css("display", "none")
    }
  })