<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Junior test</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="topbar">
        <div id="page-name">
            <h1>Product List</h1>
        </div>
        <div id="btn">
            <button type="button"><a href="addproduct.php">ADD</a></button>
            <button id="delete-product-btn" form="form1">MASS DELETE</button>
        </div>
    </div>
    <form action="delete.php" method="POST" id="form1">
        <div class="page-body">
            <?php
            include('config.php');
            $query = "SELECT * FROM products";
            $result = mysqli_query($conn, $query);
            while ($row = mysqli_fetch_array($result)) { ?>
                <div class="product-window">
                    <div>
                        <input type="checkbox" class="delete-checkbox" name=id[] value=<?= $row['id']; ?>>
                    </div>
                    <div>
                        <p><?php echo $row['sku']; ?></p>
                        <p><?php echo $row['name']; ?></p>
                        <p><?php echo $row['price']; ?> $</p>
                        <?php
                        if ($row['size'] != NULL) {
                            echo "Size: " . $row['size'] . " MB";
                        }
                        if ($row['weight'] != NULL) {
                            echo "Weight: " . $row['weight'] . " KG";
                        }
                        if (
                            $row['height'] != NULL
                            && $row['width'] != NULL
                            && $row['length'] != NULL
                        ) {
                            echo "Dimension: " . $row['height'] . "x" . $row['width'] . "x" . $row['length'];
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </form>
    </div>
    <footer>
        <p>Scandiweb Test assignment</p>
    </footer>
</body>