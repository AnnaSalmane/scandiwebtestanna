<?php

include('config.php');

$sku = $name = $price = $productType = $size = $weight = $height = $width = $length = '';
$errors = array('emptyfill' => '', 'size' => '', 'weight' => '', 'dimensions' => '');

if (isset($_POST['submit'])) {

    if (empty($_POST['sku']) || empty($_POST['name']) || empty($_POST['price'])) {
        $errors['emptyfill'] = 'Please, submit required data <br />';
    } else {
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
    }

    if (empty($_POST['productType'])) {
        $errors['emptyfill'] = 'Please, submit required data <br />';
    } else {
        if (!empty($_POST["productType"])  &&  $_POST["productType"] == "dvd") {
            if (empty($_POST['size'])) {
                $errors['emptyfill'] = 'Please, submit required data <br />';
            } elseif (!empty($_POST['size'])) {
                $productType = $_POST['productType'];
                $size = $_POST['size'];
                if (!preg_match('/^[0-9.,\b]+$/', $size)) {
                    $errors['size'] = 'Please, provide the size of indicated type';
                }
            }
        } elseif (isset($_POST["productType"])  &&  $_POST["productType"] == "book") {
            if (empty($_POST['weight'])) {
                $errors['emptyfill'] = 'Please, submit required data <br />';
            } elseif (!empty($_POST['weight'])) {
                $productType = $_POST['productType'];
                $weight = $_POST['weight'];
                if (!preg_match('/^[0-9.,\b]+$/', $weight)) {
                    $errors['weight'] = 'Please, provide the weight of indicated type';
                }
            }
        } elseif (isset($_POST["productType"])  &&  $_POST["productType"] == "furniture") {
            if (empty($_POST['height']) || empty($_POST['width']) || empty($_POST['length'])) {
                $errors['emptyfill'] = 'Please, submit required data <br />';
            } elseif (!empty($_POST['height']) || !empty($_POST['width']) || !empty($_POST['length'])) {
                $productType = $_POST['productType'];
                $height = $_POST['height'];
                $width = $_POST['width'];
                $length = $_POST['length'];
                if ((!preg_match('/^[0-9.,\b]+$/', $height)) || (!preg_match('/^[0-9.,\b]+$/', $width)) || (!preg_match('/^[0-9.,\b]+$/', $length))) {
                    $errors['dimensions'] = 'Please, provide the dimensions of indicated type';
                }
            }
        }
    }

    if (array_filter($errors)) {
    } else {
        $sku = mysqli_real_escape_string($conn, $_POST['sku']);
        $name = mysqli_real_escape_string($conn, $_POST['name']);
        $price = mysqli_real_escape_string($conn, $_POST['price']);
        $productType = mysqli_real_escape_string($conn, $_POST['productType']);
        $size = mysqli_real_escape_string($conn, $_POST['size']);
        $weight = mysqli_real_escape_string($conn, $_POST['weight']);
        $height = mysqli_real_escape_string($conn, $_POST['height']);
        $width = mysqli_real_escape_string($conn, $_POST['width']);
        $length = mysqli_real_escape_string($conn, $_POST['length']);

        $sql = "INSERT INTO products(sku, name, price, productType, size, weight, height, width, length) VALUES ('$sku', '$name', '$price', '$productType', '$size', '$weight', '$height', '$width', '$length')";

        if (mysqli_query($conn, $sql)) {

            header('Location: index.php');
        } else {

            echo 'query error: ' . mysqli_error($conn);
        }
    }
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <div class="topbar">
        <div id="page-name">
            <h1>Product Add</h1>
        </div>
        <div id="btn">
            <button type="submit" name="submit" value="submit" form="product_form">Save</button>
            <button type="button"><a href="index.php">Cancel</a></button>
        </div>
    </div>

    <form action="addproduct.php" method="POST" id="product_form">

        <div class="red-text"><?php echo $errors['emptyfill']; ?></div>
        <div class="red-text"><?php echo $errors['size']; ?></div>
        <div class="red-text"><?php echo $errors['weight']; ?></div>
        <div class="red-text"><?php echo $errors['dimensions']; ?></div>

        <div class="input">
            <label for="sku">SKU</label>
            <input type="text" name="sku" id="sku" value="<?php echo $sku ?>">
        </div>

        <div class="input">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="<?php echo $name ?>">
        </div>

        <div class="input">
            <label for="price">Price ($)</label>
            <input type="number" name="price" id="price" value="<?php echo $price ?>">
        </div>

        <div class="switcher">
            <label for="productType">Type Switcher</label>
            <select id="productType" name="productType" value="<?php echo $productType ?>">
                <option selected disabled>Type Switcher</option>
                <option value="dvd">DVD</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>

            <span id="dvd" style="display:none">
                <p>Please, provide size</p>
                <div class="input">
                    <label for='size'>Size (MB)</label>
                    <input type='text' name='size' id='size' value="<?php echo $size ?>">
                </div>

            </span>
            <span id="book" style="display:none">
                <p>Please, provide weight</p>
                <div class='input'>
                    <label for='weight'>Weight (KG)</label>
                    <input type='text' name='weight' id='weight' value="<?php echo $weight ?>">
                </div>
            </span>
            <span id="furniture" style="display:none">
                <p>Please, provide dimensions</p>
                <div class='input'>
                    <label for='height'>Height (CM)</label>
                    <input type='text' name='height' id='height' value="<?php echo $height ?>">
                </div>

                <div class='input'>
                    <label for='width'>Width (CM)</label>
                    <input type='text' name='width' id='width' value="<?php echo $width ?>">
                </div>

                <div class='input'>
                    <label for='length'>Length (CM)</label>
                    <input type='text' name='length' id='length' value="<?php echo $length ?>">
                </div>
            </span>

        </div>
    </form>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="switcher.js"></script>

    <footer>
        <p>Scandiweb Test assignment</p>
    </footer>
</body>